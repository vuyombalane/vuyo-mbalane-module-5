import 'package:flutter/material.dart';
import 'package:module_3/splash_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(
  apiKey: "AIzaSyDJ3KqhFXp7dkgSgar4XVanfHLgmQR-L3M",
  authDomain: "my-salon-41b8a.firebaseapp.com",
  projectId: "my-salon-41b8a",
  storageBucket: "my-salon-41b8a.appspot.com",
  messagingSenderId: "498284179842",
  appId: "1:498284179842:web:6046b58e60f425c90b5f9b"
    )
  );
  
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}